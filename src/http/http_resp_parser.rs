use core::ops::RangeInclusive as RngI;
use core::str::FromStr;
use std::collections::HashMap;

fn fn_r(x: &u8) -> bool { *x == b'\r' }
fn fn_n(x: &u8) -> bool { *x == b'\n' }
fn fn_sp(x: &u8) -> bool { *x == b' ' }
// fn fn_sl(x: &u8) -> bool { *x == b'/' }
fn fn_fld_nm(x: &u8) -> bool { (*x == b'-') | (*x == b'.') | x.is_ascii_alphanumeric() }
fn fn_txt(x: &u8) -> bool { (b'\t'..=12).contains(x) | (14..=255).contains(x) }

#[derive(Debug)]
pub struct HTTPResp<'a> {
    status: i32,
    fields: HashMap<&'a str, &'a str>,
    body: Option<&'a[u8]>,
}

impl<'a> HTTPResp<'a> {
    pub fn status(&self) -> i32 {
        self.status
    }
    pub fn field(&self, nm: &str) -> Option<&str> {
        self.fields.get(nm).copied()
    }
    pub fn body(&self) -> Option<&'a[u8]> {
        self.body.clone()
    }
}
 
struct HTTPRespParser<'a> {
    data: &'a[u8],
    pos: usize
}

impl<'a> HTTPRespParser<'a> {
    fn new(data: &'a[u8]) -> Self {
        Self { data, pos: 0 }
    }
    fn txt_(&self, rng: core::ops::Range<usize>) -> Option<&str> {
        core::str::from_utf8(&self.data[rng]).ok()
    }
    fn end_(&self) -> bool {
        self.pos >= self.data.len()
    }
    fn next_(&mut self, f: fn(x:&u8)->bool) -> bool {
        if !self.end_() && f(&self.data[self.pos]) {
            self.pos += 1;
            true
        }
        else { false }
    }
    fn skip_(&mut self, f: fn(x:&u8)->bool) {
        while self.next_(f) { }
    }
    fn status_line(&mut self) -> Option<i32> {
        // HTTP/1.1 200 OK\r\n
        for _ in 0..4 {
            if !self.next_(u8::is_ascii_alphabetic) { return None; }
        }
        if !self.next_(|x| *x == b'/') { return None; }
        if !self.next_(u8::is_ascii_digit) { return None; }
        self.skip_(u8::is_ascii_digit);
        if !self.next_(|x| *x == b'.') { return None; }
        if !self.next_(u8::is_ascii_digit) { return None; }
        self.skip_(u8::is_ascii_digit);
        //
        if !self.next_(fn_sp) { return None;  }
        //
        let begin_pos = self.pos;
        for _ in 0..3 {
            if !self.next_(u8::is_ascii_digit) { return None; }
        }
        let code_str = self.txt_(begin_pos..self.pos).unwrap();
        let code = i32::from_str(code_str).unwrap();
        //
        if !self.next_(fn_sp) { return None;  }
        //
        self.skip_(fn_txt);
        //
        if self.end_() { return None; }
        Some(code)
    }
    fn field(&mut self) -> Option<(RngI<usize>,RngI<usize>)> {
        // Content-Length: 2534\r\n
        let begin_pos = self.pos;
        self.skip_(fn_fld_nm);
        let name_rng = if let Some(_) = self.txt_(begin_pos..self.pos) {
            begin_pos..=(self.pos-1)
        }
        else { return None; };
        if !self.next_(|x| *x == b':') { return None;  }
        self.skip_(fn_sp);
        let begin_pos = self.pos;
        self.skip_(fn_txt);
        let value_rng = if let Some(_) = self.txt_(begin_pos..self.pos) {
            begin_pos..=(self.pos-1)
        }
        else { return None; };
        if self.end_() { return None; }
        Some((name_rng,value_rng))
    }
    fn parse(&mut self) -> Result<HTTPResp<'a>,()> {
        let status = self.status_line().ok_or(())?;
        if !(self.next_(fn_r) && self.next_(fn_n)) { return Err(()); }
        //
        let mut flds = Vec::with_capacity(4);
        while !self.end_() {
            if self.next_(fn_r) && self.next_(fn_n) { break; }
            if let Some(x) = self.field() {
                flds.push(x);
                if !(self.next_(fn_r) && self.next_(fn_n)) { return Err(()); }
            }
            else { return Err(()); }
        }
        //
        let mut fields = HashMap::with_capacity(4);
        for (name_rng,value_rng) in flds {
            let name = unsafe { 
                core::str::from_utf8_unchecked(&self.data[name_rng])
            };
            let value = unsafe { 
                core::str::from_utf8_unchecked(&self.data[value_rng])
            };
            fields.insert(name,value);
        }
        let len = self.data.len();
        let rng = self.pos..=(len-1);
        let body = if rng.is_empty() { None } else { Some(&self.data[rng]) };
        //
        Ok(HTTPResp{status,fields,body})
    }
}

pub fn parse<'a>(data: &'a[u8]) -> Result<HTTPResp<'a>,()>
{
    HTTPRespParser::new(data).parse()
}
