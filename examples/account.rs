
include!("../dns_lookup.rs");

fn main()
{
    use fpdec::Decimal;
    use binance_rr::Status;

    let api_address = lookup_adress("api.binance.com").unwrap();
    let api_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let secr_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let mut builder = binance_rr::Builder::new(api_address, api_key, secr_key);
    let mut r = builder.account(true);
    println!("=== SPOT ===");
    loop {
        r.update();
        match r.get() {
            Status::Done(assets) => {
                for a in assets {
                    if a.free > Decimal::ZERO || a.locked > Decimal::ZERO {
                        println!("{}: {}, [{}]", a.asset, a.free, a.locked);
                    }
                }
                break;
            }
            Status::Wait => {}
            _ => { eprintln!("error: {}", 16); break; }
        }
    }
    let mut r = builder.margin_account();
    println!("=== CROSS ===");
    loop {
        r.update();
        match r.get() {
            Status::Done(assts) => {
                for a in assts {
                    if !(a.free > Decimal::ZERO || a.locked > Decimal::ZERO) {
                        continue;
                    }
                    let total = a.net + a.borrowed;
                    println!("{}:  {} = {} + {} | {} |", 
                        a.asset, total, a.net, a.borrowed, a.interest);
                }
                break;
            }
            Status::Wait => {}
            _ => { eprintln!("error: {}", 14); break; }
        }
    }
    let mut r = builder.isolated_account("");
    println!("=== ISOLATED ===");
    loop {
        r.update();
        match r.get() {
            Status::Done(pairs) => {
                for p in pairs {
                    let b = &p.base;
                    let q = &p.quote;
                    if !(b.total > Decimal::ZERO || q.total > Decimal::ZERO) {
                        continue;
                    }
                    println!("-- {} --", p.symb);
                    println!("  {}:  {} = {} + {} | {} |", 
                        b.asset, b.total, b.net, b.borrowed, b.interest);
                    println!("  {}:  {} = {} + {} | {} |", 
                        q.asset, q.total, q.net, q.borrowed, q.interest);
                }
                break;
            }
            Status::Wait => {}
            _ => { eprintln!("error: {}", 14); break; }
        }
    }
}
