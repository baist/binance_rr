
include!("../dns_lookup.rs");

fn main()
{
    use fpdec::Decimal;
    use binance_rr::Status;

    let api_address = lookup_adress("api.binance.com").unwrap();
    let api_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let secr_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let mut builder = binance_rr::Builder::new(api_address, api_key, secr_key);
    let mut r = builder.trade_fee("");
    loop {
        r.update();
        match r.get() {
            Status::Done(fees) => {
                for f in fees {
                    if Decimal::ZERO == f.maker && Decimal::ZERO == f.taker {
                        println!("{}: {}, {}", f.symb, f.maker, f.taker);
                    }
                }
                break;
            }
            Status::Wait => {}
            _ => { eprintln!("error: {}", 16); break; }
        }
    }
}
