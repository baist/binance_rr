use std::net::SocketAddr;

mod http;

use binance_api::types::{ExchInfo,Kline,Interval,Side,Order,
    Fee,Asset,IsolatedPair,TimeInForce,MarginAsset};
use http::Resp as HTTPResp;
use http::Status as HTTPStatus;
use fpdec::Decimal;

const HOST: &str = "api.binance.com";

fn timestamp() -> u64
{
    use std::time::{SystemTime, UNIX_EPOCH};

    let ts = 
        SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();
    assert!(ts < (u64::MAX as _));
    ts as u64
}

pub struct Builder {
    addr: SocketAddr,
    buffer: Vec<u8>,
    req: binance_api::req::Req
}

impl Builder {
    pub fn new(addr: SocketAddr, a: &str, b: &str) -> Self {
        let mut req = binance_api::req::Req::new(HOST);
        req.set_api_key(a);
        req.set_secr_key(b);
        Self {
            addr,
            buffer: Vec::with_capacity(256),
            req
        }
    }
    pub fn server_time(&mut self) -> Resp<u64>
    {
        self.buffer.clear();
        self.req.server_time(&mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let time = binance_api::res::server_time(data)?;
                Ok(time)
            }
        )
    }
    pub fn exchange_info(&mut self, symb: &str) -> Resp<Vec<ExchInfo>>
    {
        self.buffer.clear();
        self.req.exchange_info(symb, &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut pairs = Vec::with_capacity(8);
                binance_api::res::exchange_info(data, &mut pairs)?;
                if pairs.len() > 0 {  Ok(pairs)  }
                else {  Err(line!())  }
            },
        )
    }
    pub fn klines(&mut self, symb: &str, intrvl: Interval, 
            start: u64, end: u64, limit: usize) -> Resp<Vec<Kline>>
    {
        self.buffer.clear();
        self.req.klines(symb, intrvl, start, end,
            limit, &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut klines = Vec::with_capacity(16);
                binance_api::res::klines(data, &mut klines)?;
                Ok(klines)
            },
        )
    }
    pub fn account(&mut self, omit_zero_balances: bool) -> Resp<Vec<Asset>>
    {
        self.buffer.clear();
        self.req.account(omit_zero_balances, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut balances = Vec::with_capacity(1);
                binance_api::res::account(data, &mut balances)?;
                Ok(balances)
            },
        )
    }
    pub fn trade_fee(&mut self, symb: &str) -> Resp<Vec<Fee>>
    {
        self.buffer.clear();
        self.req.trade_fee(symb, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut fees = Vec::with_capacity(1);
                binance_api::res::trade_fee(data, &mut fees)?;
                if fees.len() > 0 {  Ok(fees)  }
                else {  Err(line!())  }
            },
        )
    }
    pub fn margin_account(&mut self) -> Resp<Vec<MarginAsset>>
    {
        self.buffer.clear();
        self.req.margin_account(timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut pairs = Vec::with_capacity(1);
                binance_api::res::margin_account(data, &mut pairs)?;
                Ok(pairs)
            },
        )
    }
    pub fn margin_loan(&mut self, isolated: bool, symb: &str, asset: &str, amount: Decimal) -> Resp<()>
    {
        self.buffer.clear();
        self.req.margin_loan(isolated, symb, asset, amount, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::margin_loan(data)
            }
        )
    }
    pub fn margin_repay(&mut self, isolated: bool, symb: &str, asset: &str, amount: Decimal) -> Resp<()>
    {
        self.buffer.clear();
        self.req.margin_repay(isolated, symb, asset, amount, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::margin_repay(data)
            },
        )
    }
    pub fn margin_new_order_m(&mut self, isolated: bool, symb: &str, 
        side: Side, qty: Decimal) -> Resp<Order>
    {
        self.buffer.clear();
        self.req.margin_new_order_m(isolated, symb, side, qty, timestamp(), 
            &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::margin_new_order(data)
            }
        )
    }
    pub fn margin_new_order_l(&mut self, isolated: bool, symb: &str, 
        side: Side, price: Decimal, qty: Decimal,
        time_in_force: TimeInForce) -> Resp<Order>
    {
        self.buffer.clear();
        self.req.margin_new_order_l(isolated, symb,
            side, price, qty, time_in_force, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::margin_new_order(data)
            }
        )
    }
    pub fn margin_cancel_order(&mut self, isolated: bool,
        symb: &str, order_id: u64, clo_id: &str) -> Resp<Order>
    {
        self.buffer.clear();
        self.req.margin_cancel_order(isolated, symb, order_id,
            clo_id, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::margin_cancel_order(data)
            }
        )
    }
    pub fn margin_open_orders(&mut self, isolated: bool, symb: &str) -> Resp<Vec<Order>>
    {
        self.buffer.clear();
        self.req.margin_open_orders(isolated,
            symb, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut orders = Vec::with_capacity(2);
                binance_api::res::margin_open_orders(data, &mut orders)?;
                Ok(orders)
            },
        )
    }
    pub fn margin_all_orders(&mut self, isolated: bool, symb: &str,
        order_id: u64, start: u64, end: u64, limit: usize) -> Resp<Vec<Order>>
    {
        self.buffer.clear();
        self.req.margin_all_orders(isolated, symb, order_id, start, end,
            limit, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut orders = Vec::with_capacity(2);
                binance_api::res::margin_all_orders(data, &mut orders)?;
                Ok(orders)
            },
        )
    }
    pub fn isolated_account(&mut self, symb: &str) -> Resp<Vec<IsolatedPair>>
    {
        self.buffer.clear();
        self.req.isolated_acc_info(symb, timestamp(), &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                let mut pairs = Vec::with_capacity(1);
                binance_api::res::isolated_account(data, &mut pairs)?;
                Ok(pairs)
            },
        )
    }

    pub fn spot_create_list_key(&mut self) -> Resp<String>
    {
        self.buffer.clear();
        self.req.spot_list_key_create(&mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::list_key_create(data)
            },
        )
    }

    pub fn spot_ping_list_key(&mut self, list_key: &str) -> Resp<()>
    {
        self.buffer.clear();
        self.req.spot_list_key_ping(list_key, &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::list_key_ping(data)
            },
        )
    }

    pub fn isolated_create_list_key(&mut self, symb: &str) -> Resp<String>
    {
        self.buffer.clear();
        self.req.isolated_list_key_create(symb, &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::list_key_create(data)
            },
        )
    }

    pub fn isolated_ping_list_key(&mut self, symb: &str, list_key: &str) -> Resp<()>
    {
        self.buffer.clear();
        self.req.isolated_list_key_ping(symb, list_key, &mut self.buffer).unwrap();
        self.make_req(
            | data | {
                binance_api::res::list_key_ping(data)
            },
        )
    }

    fn make_req<T>(&mut self, parser_func: ParserFunc<T>) -> Resp<T>
    {
        let http_resp = Some(http::request(1,
            self.addr.clone(), HOST, None, &self.buffer));
        Resp {
            http_resp,
            parser_func,
            result: Status::Wait
        }
    }
}


#[derive(Debug)]
pub enum BadStatus {
    // Network
    Timeout,
    Connection,
    // Client problem
    BadReq,
    InvalidTS,
    InvalidSig,
    ParamEmpty,
    BadPrecision,
    InvalidInterval,
    InvalidSymbol,
    //
    Forbidden,
    NotFound,
    Conflict,
    TooManyReq,
    IamTeapot,
    // Server problem
    HasIssues
}

fn conv_e_code_to_enum(body: &[u8]) -> BadStatus
{
    let (c,m) = binance_api::res::error_msg(body);
    if !m.is_empty() { eprintln!("[e]  error({}): {}", c, m); }
    match c {
        1021 => BadStatus::InvalidTS,
        1022 => BadStatus::InvalidSig,
        1105 => BadStatus::ParamEmpty,
        1111 => BadStatus::BadPrecision,
        1120 => BadStatus::InvalidInterval,
        1121 => BadStatus::InvalidSymbol,
        _ => BadStatus::BadReq,
    }
}

pub enum Status<T> {
    Done(T),
    Wait,
    Bad(BadStatus)
}

type ParserFunc<T> = fn(&[u8]) -> Result<T, u32>;

pub struct Resp<T> {
    http_resp: Option<HTTPResp>,
    parser_func: ParserFunc<T>,
    result: Status<T>
}

impl<T> Resp<T> {
    pub fn is_done(&self) -> bool
    {
        match self.result {
            Status::Done(_) => { true }
            _ => { false }
        }
    }
    pub fn is_wait(&self) -> bool
    {
        match self.result {
            Status::Wait => { true }
            _ => { false }
        }
    }
    pub fn get(&self) -> &Status<T>
    {
        &self.result
    }
    pub fn update(&mut self)
    {
        // use binance_api::res::error_msg;
        use http::BadStatus as BS_;

        let mut resp =
            if let Some(x) = self.http_resp.take() { x } else { return; };
        resp.update();
        match resp.status() {
            HTTPStatus::Done => {
                match resp.get_status_code() {
                    Some(200) => {
                        if let Some(body) = resp.get_body() {
                            self.parse_resp(&body);
                        }
                    }
                    Some(302) => {
                        eprintln!("error: resource is moved");
                        self.result = Status::Bad(BadStatus::Connection);
                    }
                    Some(400) => {
                        if let Some(body) = resp.get_body() {
                            let stts = conv_e_code_to_enum(body);
                            self.result = Status::Bad(stts);
                        }
                        else {
                            self.result = Status::Bad(BadStatus::BadReq);
                        }
                    }
                    Some(403) => {
                        self.result = Status::Bad(BadStatus::Forbidden);
                    }
                    Some(404) => {
                        self.result = Status::Bad(BadStatus::NotFound);
                    }
                    Some(409) => {
                        self.result = Status::Bad(BadStatus::Conflict);
                    }
                    Some(418) => {
                        self.result = Status::Bad(BadStatus::IamTeapot);
                    }
                    Some(429) => {
                        self.result = Status::Bad(BadStatus::TooManyReq);
                    }
                    Some(500) => {
                        self.result = Status::Bad(BadStatus::HasIssues);
                    }
                    Some(code) => {
                        eprintln!("{}:{}: unsupport status code {}", file!(), line!(), code);
                        self.result = Status::Bad(BadStatus::Connection);
                    }
                    None => {
                        eprintln!("{}:{}: bad HTTP response", file!(), line!());
                        self.result = Status::Bad(BadStatus::Connection);
                    }
                }
            }
            HTTPStatus::InProgress => {
                self.http_resp = Some(resp);
                self.result = Status::Wait;
            }
            HTTPStatus::Bad(t) => {
                match t {
                    BS_::Timeout => {
                        self.result = Status::Bad(BadStatus::Timeout);
                    }
                    BS_::Connection => {
                        self.result = Status::Bad(BadStatus::Connection);
                    }
                }
            }
        }
    }
    fn parse_resp(&mut self, data: &[u8])
    {
        self.result = match (self.parser_func)(data) {
            Ok(data) => {
                Status::Done(data)
            }
            Err(e) => {
                eprintln!("{}:{}: error {}", file!(), line!(), e);
                Status::Bad(BadStatus::Connection)
            }
        };
    }
}
