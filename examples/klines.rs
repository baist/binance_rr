
include!("../dns_lookup.rs");

use binance_api::types::Kline;

fn get_kl(builder: &mut binance_rr::Builder, symb: &str) -> Result<Kline,()>
{
    use binance_api::types::{Interval};
    use binance_rr::Status;

    let mut r = builder.klines(symb, Interval::I_1d, 0, 0, 1);
    loop {
        r.update();
        match r.get() {
            Status::Done(kls) => {
                let mut ret = Err(());
                for kl in kls {
                    ret = Ok(kl.clone());
                }
                return ret;
            }
            Status::Wait => {}
            _ => { eprintln!("error: {}", 14); return Err(()); }
        }
    }
}

fn main()
{
    let symbs = [
        "AEURUSDT",
        "BNBFDUSD",
        "BTCAEUR",
        "BTCFDUSD",
        "BTCUSDP",
        "BUSDUSDT",
        "DOGEFDUSD",
        "ETHFDUSD",
        "EURAEUR",
        "FDUSDBUSD",
        "FDUSDUSDT",
        "LINKFDUSD",
        "PAXBUSD",
        "SOLFDUSD",
        "SUSDUSDT",
        "TUSDBUSD",
        "TUSDUSDT",
        "USDCUSDT",
        "USDPUSDT",
        "USTBUSD",
        "USTUSDT",
        "XRPFDUSD",
        "PIXELFDUSD",
        "HOTUSDT",
    ];
    let api_address = lookup_adress("api.binance.com").unwrap();
    let api_key = "";
    let secr_key = "";
    let mut builder = binance_rr::Builder::new(api_address, api_key, secr_key);
    for s in symbs {
        let kline = get_kl(&mut builder, s).unwrap();
        println!("{}: {}", s, kline.close_price);
    }
}
