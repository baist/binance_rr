
include!("../dns_lookup.rs");

use binance_api::types::Kline;

fn get_kl(builder: &mut binance_rr::Builder, symb: &str) -> Result<Kline,()>
{
    use binance_api::types::{Interval};
    use binance_rr::Status;

    let mut r = builder.klines(symb, Interval::I_1d, 0, 0, 2);
    loop {
        r.update();
        match r.get() {
            Status::Done(kls) => {
                let mut ret = Err(());
                if kls.len() > 1 {
                    ret = Ok(kls[kls.len() - 2].clone());
                }
                return ret;
            }
            Status::Wait => {}
            _ => { eprintln!("error: {}", 14); return Err(()); }
        }
    }
}

fn main()
{
    // use fpdec::{Dec,Decimal};

    let symbs = [
        "AEURUSDT",
        "BNBFDUSD",
        "BTCAEUR",
        "BTCFDUSD",
        "BTCUSDP",
        "BUSDUSDT",
        "DOGEFDUSD",
        "ETHFDUSD",
        "EURAEUR",
        "FDUSDBUSD",
        "FDUSDUSDT",
        "LINKFDUSD",
        "PAXBUSD",
        "SOLFDUSD",
        "SUSDUSDT",
        "TUSDBUSD",
        "TUSDUSDT",
        "USDCUSDT",
        "USDPUSDT",
        "USTBUSD",
        "USTUSDT",
        "XRPFDUSD",
    ];
    let api_address = lookup_adress("api.binance.com").unwrap();
    let api_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let secr_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let mut builder = binance_rr::Builder::new(api_address, api_key, secr_key);
    for s in symbs {
        let kline = get_kl(&mut builder, s).unwrap();
        let avg_price = kline.low_price + (kline.high_price - kline.low_price) / 2;
        let num_trades = kline.num_trades;
        let volume = kline.quote_volume;
        let profit = 100 / kline.low_price * kline.high_price - 100;
        println!("{},{},{},{},{}", s, avg_price, num_trades, volume, profit);
    }
}
