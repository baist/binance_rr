include!("../dns_lookup.rs");

fn main()
{
    use binance_rr::Status;

    let api_address = lookup_adress("api.binance.com").unwrap();
    let api_key =  "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let secr_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    let mut builder = binance_rr::Builder::new(api_address, api_key, secr_key);
    let mut r = builder.spot_create_list_key();
    loop {
        r.update();
        match r.get() {
            Status::Done(s) => {
                println!("{}", s);
                break;
            }
            Status::Wait => {}
            Status::Bad(e) => { eprintln!("error: {:?}", e); break; }
        }
    }
}

