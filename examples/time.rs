
include!("../dns_lookup.rs");

fn timestamp() -> u64
{
    use std::time::{SystemTime, UNIX_EPOCH};

    let ts = 
        SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();
    assert!(ts < (u64::MAX as _));
    ts as u64
}

fn main()
{
    use binance_rr::{Status,BadStatus};

    let api_address = lookup_adress("api.binance.com").unwrap();
    let api_key = "";
    let secr_key = "";
    let mut builder = binance_rr::Builder::new(api_address, api_key, secr_key);
    let ts = timestamp();
    println!("local:  {} ms", ts);
    let mut r = builder.server_time();
    loop {
        r.update();
        match r.get() {
            Status::Done(t) => {
                let ts = timestamp();
                println!("server: {} ms", t);
                println!("local:  {} ms", ts);
                break;
            }
            Status::Wait => {}
            Status::Bad(_) => { eprintln!("error: {}", 16); break; }
        }
    }
}