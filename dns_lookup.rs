use std::net::SocketAddr as SocketAddr__;

fn dns_lookup_lookup(service: &str, host: &str, ret: &mut[SocketAddr__]) -> Result<usize,u8>
{
    use core::ptr;
    use std::ffi::CString;
    use std::net;
    use libc::{AF_INET,AF_INET6,AF_UNSPEC,c_int,addrinfo};

    let cstr_sss = CString::new(service).unwrap();
    let cstr_host = CString::new(host).unwrap();
    let c_sss = cstr_sss.as_ptr();
    let c_host = cstr_host.as_ptr();

    let mut c_hints: addrinfo = unsafe { core::mem::zeroed() };
    c_hints.ai_flags = 0; // AI_PASSIVE;
    c_hints.ai_family = AF_UNSPEC;
    c_hints.ai_socktype = libc::SOCK_STREAM;
    c_hints.ai_protocol = 0;

    let mut index = 0;

    let mut res = ptr::null_mut();

    let status = unsafe { libc::getaddrinfo(c_host, c_sss, &mut c_hints, &mut res) };
    if status != 0 {  return Err(1);  }

    let mut p_addr_i = res;
    while !p_addr_i.is_null() {
        if index >= ret.len() { break; }
        let addr_i = unsafe { *p_addr_i };
        match addr_i.ai_family {
            AF_INET => {
                let sa = unsafe { *(addr_i.ai_addr as *mut libc::sockaddr_in) };
                debug_assert_eq!(sa.sin_family as c_int, AF_INET);
                let arr_addr = sa.sin_addr.s_addr.to_le_bytes();
                let sock_addr = SocketAddr__::new(
                    net::IpAddr::V4(
                        net::Ipv4Addr::new(
                            arr_addr[0],arr_addr[1],
                            arr_addr[2],arr_addr[3])),
                    u16::from_be(sa.sin_port));
                ret[index] = sock_addr;
                index += 1;
            }
            AF_INET6 => {
                let sa = unsafe { *(addr_i.ai_addr as *mut libc::sockaddr_in6) };
                debug_assert_eq!(sa.sin6_family as c_int, AF_INET6);
                let arr_addr = sa.sin6_addr.s6_addr;
                let sock_addr = SocketAddr__::new(
                    net::IpAddr::V6(
                        net::Ipv6Addr::new(
                            u16::from_be_bytes([arr_addr[0],arr_addr[1]]),
                            u16::from_be_bytes([arr_addr[2],arr_addr[3]]),
                            u16::from_be_bytes([arr_addr[4],arr_addr[5]]),
                            u16::from_be_bytes([arr_addr[6],arr_addr[7]]),
                            u16::from_be_bytes([arr_addr[8],arr_addr[9]]),
                            u16::from_be_bytes([arr_addr[10],arr_addr[11]]),
                            u16::from_be_bytes([arr_addr[12],arr_addr[13]]),
                            u16::from_be_bytes([arr_addr[14],arr_addr[15]]),
                            )),
                    u16::from_be(sa.sin6_port));
                ret[index] = sock_addr;
                index += 1;
            }
            _ => { return Err(2); }
        }
        p_addr_i = addr_i.ai_next as *mut addrinfo;
    }

    unsafe { libc::freeaddrinfo(res); }

    Ok(index)
}

pub fn lookup_adress(host: &str) -> Option<SocketAddr__>
{
    let mut addrs = Vec::with_capacity(8);
    unsafe { addrs.set_len(8); }
    match dns_lookup_lookup("https", host, &mut addrs) {
        Ok(0) => {
            return None;
            }
        Ok(_l) => {
            return Some(addrs[0].clone());
        }
        Err(e) => {
            eprintln!("  error: name resolving failed {:?}", e);
            return None;
        }
    }
}
