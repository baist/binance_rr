
include!("../dns_lookup.rs");

fn main()
{
    use binance_api::types::ExchStatus;
    use binance_rr::Status;

    let api_address = lookup_adress("api.binance.com").unwrap();
    let api_key = "";
    let secr_key = "";
    let mut builder = binance_rr::Builder::new(api_address, api_key, secr_key);
    let mut r = builder.exchange_info("");
    loop {
        r.update();
        match r.get() {
            Status::Done(pairs) => {
                for p in pairs {
                    match p.status {
                        ExchStatus::Trading => {
                            println!("{}/{}", p.base, p.quote);
                        }
                        _ => {}
                    }
                }
                break;
            }
            Status::Wait => {}
            _ => { eprintln!("error: {}", 13); break; }
        }
    }
}
