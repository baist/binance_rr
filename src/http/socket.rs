use std::io;
use std::net::SocketAddr;
use std::net::IpAddr;

use libc::{AF_INET,SOCK_STREAM};

macro_rules! scall {
    ($fn: ident ( $($arg: expr),* $(,)* ) ) => {{
        #[allow(unused_unsafe)]
        let res = unsafe { libc::$fn($($arg, )*) };
        if res == -1 {
            Err(std::io::Error::last_os_error())
        } else {
            Ok(res)
        }
    }};
}

const MAX_BUF_LEN: usize = libc::ssize_t::MAX as usize;

#[derive(Debug)]
pub struct Socket {
    fd: libc::c_int
}

impl Socket {
    pub fn create() -> io::Result<Self>
    {
        let fd = scall!(socket(AF_INET, SOCK_STREAM, 0))?;
        let previous = scall!(fcntl(fd, libc::F_GETFL))?;
        let new = previous | libc::O_NONBLOCK;
        if new != previous {
            scall!(fcntl(fd, libc::F_SETFL, new))?;
        }
        Ok(Self {
            fd
        })
    }
    pub fn connect(&self, sock_addr: SocketAddr) -> io::Result<()>
    {
        use core::mem::size_of_val;

        let octets = match sock_addr.ip() {
            IpAddr::V4(ip) => { ip.octets() }
            _ => {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Unsupported IP format",
                    ));
            }
        };
        let s_addr = u32::from_le_bytes(octets);
        let port = sock_addr.port().to_be();
        let sa_in = libc::sockaddr_in {
            sin_family: AF_INET as libc::sa_family_t,   // sa_family_t,
            sin_port: port,                             // in_port_t,
            sin_addr: libc::in_addr { s_addr },         // in_addr,
            sin_zero: [0u8; 8],                         // [u8; 8],
        };
        let res = scall!(connect(self.fd,
            &sa_in as *const _ as *const _,
            size_of_val(&sa_in) as libc::socklen_t));
        match res {
            Ok(_) => {},
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {}
            #[cfg(unix)]
            Err(ref e) if e.raw_os_error() == Some(libc::EINPROGRESS) => {}
            Err(e) => return Err(e),
        }
        Ok(())
    }
    #[allow(dead_code)]
    pub fn is_ready(&self) -> io::Result<u8>
    {
        use core::mem::{MaybeUninit,size_of};
        use libc::{POLLIN,POLLOUT,POLLHUP,POLLERR,SOL_SOCKET,SO_ERROR,pollfd};

        let mut pollfd = pollfd {
            fd: self.fd,
            events: POLLIN | POLLOUT,
            revents: 0,
        };

        let timeout = 0;
        match scall!(poll(&mut pollfd, 1, timeout)) {
            Ok(0) => {
                return Err(io::Error::from(io::ErrorKind::WouldBlock));
            }
            Ok(1) => {
                // Error or hang up indicates an error (or failure to connect).
                if (pollfd.revents & POLLHUP) != 0 ||
                    (pollfd.revents & POLLERR) != 0
                {
                    let mut payload: MaybeUninit<libc::c_int> = MaybeUninit::uninit();
                    let mut len = size_of::<libc::c_int>() as libc::socklen_t;
                    let res = scall!(getsockopt(
                        self.fd, SOL_SOCKET, SO_ERROR,
                        payload.as_mut_ptr().cast(), &mut len)).map(|_| {
                            debug_assert_eq!(len as usize, size_of::<libc::c_int>());
                            // Safety: `getsockopt` initialised `payload` for us.
                            unsafe { payload.assume_init() }
                        });
                    match res {
                        Ok(0) => {
                            return Err(io::Error::new(
                                io::ErrorKind::Other,
                                "no error set after POLLHUP",
                            ));
                        }
                        Ok(err_no) => {
                            return Err(io::Error::from_raw_os_error(err_no))
                        }
                        Err(err) => { return Err(err); }
                    }
                }
                let mut res = u8::from((pollfd.revents & POLLIN) == POLLIN);
                res |= u8::from((pollfd.revents & POLLOUT) == POLLOUT) << 1;
                return Ok(res);
            }
            Ok(_) => {
                panic!("unexpected result");
            }
            // Got interrupted, try again.
            Err(ref err) if err.kind() == io::ErrorKind::Interrupted => {
                return Err(io::Error::from(io::ErrorKind::WouldBlock));
            }
            Err(err) => {
                return Err(err) 
            }
        }
    }

    pub fn shutdown(&self) -> io::Result<()>
    {
        let how = libc::SHUT_RDWR;
        scall!(shutdown(self.fd, how)).map(|_| ())
    }
}

impl io::Read for Socket {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize>
    {
        let flags = 0;
        scall!(recv(
                self.fd,
                buf.as_mut_ptr().cast(),
                buf.len().min(MAX_BUF_LEN),
                flags,
            )).map(|n| n as usize)
    }
}

impl io::Write for Socket {
    // Required methods
    fn write(&mut self, buf: &[u8]) -> io::Result<usize>
    {
        let flags = 0;
        scall!(send(
                self.fd,
                buf.as_ptr().cast(),
                buf.len().min(MAX_BUF_LEN),
                flags,
            ))
            .map(|n| n as usize)
    }
    fn flush(&mut self) -> io::Result<()>
    {
        Ok(())
    }
}

impl Drop for Socket {
    fn drop(&mut self) {
        let _ = self.shutdown();
    }
}

