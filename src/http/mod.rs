use core::time::Duration;
use std::time::Instant;
use std::net::{ SocketAddr};
use native_tls::{TlsStream,MidHandshakeTlsStream};

mod socket;
mod http_resp_parser;

use socket::Socket;

pub fn request(_id: usize, sock_addr: SocketAddr, host: &str, 
    timeout: Option<Duration>, data: &[u8]) -> Resp
{
    let data = data.to_vec();
    let sock_addr = sock_addr.into();
    let socket = Socket::create().unwrap();
    let stage = Stage::S0(sock_addr, socket);

    Resp {
        host: host.to_string(),
        timeout,
        time: Instant::now(),
        status: Status::InProgress,
        stage,
        data,
        status_code: None,
        body: None
    }
}

#[derive(Clone,Debug)]
pub enum BadStatus {
    Timeout,
    Connection
}

#[derive(Clone,Debug)]
pub enum Status {
    Done,
    InProgress,
    Bad(BadStatus)
}

type TlsSocket = TlsStream<Socket>;

enum Stage {
    SE,
    S0(SocketAddr,Socket),
    S1(MidHandshakeTlsStream<Socket>),
    S2(TlsSocket),
    S3(TlsSocket),
    S4(TlsSocket,usize),
    S5
}

pub struct Resp {
    host: String,
    timeout: Option<Duration>,
    time: Instant,
    status: Status,
    stage: Stage,
    data: Vec<u8>,
    status_code: Option<i32>,
    body: Option<Vec<u8>>
}

impl Resp {
    pub fn status(&self) -> Status {
        self.status.clone()
    }
    pub fn get_status_code(&self) -> Option<i32>
    {
        self.status_code
    }
    pub fn get_body(&self) -> Option<&[u8]>
    {
        self.body.as_ref().map(|x|x.as_slice())
    }
    pub fn update(&mut self)
    {
        use core::mem;

        if let Some(timeout) = self.timeout {
            match &self.stage {
                Stage::SE | Stage::S5 => {}
                _ => {
                    if self.time.elapsed() >= timeout {
                        self.stage = Stage::SE;
                        self.status = Status::Bad(BadStatus::Timeout);
                        return;
                    }
                }
            }
        }

        let s = mem::replace(&mut self.stage, Stage::SE);

        match s {
            Stage::SE => {
                self.stage = Stage::SE;
            }
            Stage::S0(sock_addr,socket) => {
                self.stage0(sock_addr,socket);
            }
            Stage::S1(m) => {
                self.stage1(m);
            }
            Stage::S2(sock) => {
                self.stage2(sock);
            }
            Stage::S3(sock) => {
                self.stage3(sock);
            }
            Stage::S4(sock,remain) => {
                self.stage4(sock,remain);
            }
            Stage::S5 => {
                self.stage = Stage::S5;
            }
        }
    }

    fn stage0(&mut self, sock_addr: SocketAddr, socket: Socket)
    {
        use native_tls::{TlsConnector,HandshakeError};

        match socket.connect(sock_addr) {
            Ok(()) => {
                let tls_conn = TlsConnector::new().unwrap();
                let ret = tls_conn.connect(&self.host, socket);
                match ret {
                    Ok(s) => {
                        self.stage = Stage::S2(s);
                        self.status = Status::InProgress;
                    }
                    Err(HandshakeError::Failure(_err)) => {
                        self.stage = Stage::SE;
                        self.status = Status::Bad(BadStatus::Connection);
                    }
                    Err(HandshakeError::WouldBlock(m)) => {
                        self.stage = Stage::S1(m);
                        self.status = Status::InProgress;
                    }
                }
            }
            Err(e) => {
				use std::error::Error;
                dbg!(e.kind());
                dbg!(e.source());
                dbg!(e.raw_os_error());
                dbg!(e);
                self.stage = Stage::SE;
                self.status = Status::Bad(BadStatus::Connection);
            }
        }
    }

    fn stage1(&mut self, m: MidHandshakeTlsStream<Socket>)
    {
        use native_tls::HandshakeError;

        let ret = m.handshake();
        match ret {
            Ok(s) => {
                self.stage = Stage::S2(s);
                self.status = Status::InProgress;
            }
            Err(HandshakeError::Failure(_err)) => {
                self.stage = Stage::SE;
                self.status = Status::Bad(BadStatus::Connection);
            }
            Err(HandshakeError::WouldBlock(m)) => {
                self.stage = Stage::S1(m);
                self.status = Status::InProgress;
            }
        }
    }

    fn stage2(&mut self, mut sock: TlsSocket)
    {
        use std::io::Write;
        sock.write(&self.data).unwrap();
        self.stage = Stage::S3(sock);
        self.status = Status::InProgress;
    }

    fn stage3(&mut self, mut sock: TlsSocket)
    {
        use std::io::{Read,ErrorKind};

        let mut buf = Vec::with_capacity(4096);
        buf.resize(4096, 0);
        match sock.read(&mut buf) {
            Ok(len) => {
                match http_resp_parser::parse(&buf[0..len]) {
                    Ok(resp) => {
                        let mut content_length = 0;
                        self.status_code = Some(resp.status());
                        if let Some(value) = resp.field("Content-Length") {
                            match str::parse::<usize>(value.trim()) {
                                Ok(length) => { content_length = length; }
                                Err(_) => { }
                            }
                        }
                        let mut body_len = 0;
                        if let Some(data) = resp.body() {
                            self.body = Some(data.to_vec());
                            body_len = data.len();
                        }
                        if body_len == content_length {
                            self.stage = Stage::S5;
                            self.status = Status::Done;
                        }
                        else if body_len < content_length {
                            // chunked
                            let remain = content_length - body_len;
                            self.stage = Stage::S4(sock, remain);
                            self.status = Status::InProgress;
                        }
                        else if body_len > content_length {
                            self.stage = Stage::SE;
                            self.status = Status::Bad(BadStatus::Connection);
                        }
                    }
                    Err(_) => {
                        self.stage = Stage::SE;
                        self.status = Status::Bad(BadStatus::Connection);
                    }
                }
            }
            Err(e) => {
                match e.kind() {
                    ErrorKind::WouldBlock => {
                        self.stage = Stage::S3(sock);
                        self.status = Status::InProgress;
                    }
                    _ => {
                        dbg!(e);
                        self.stage = Stage::SE;
                        self.status = Status::Bad(BadStatus::Connection);
                    }
                }
            }
        }
    }

    fn stage4(&mut self, mut sock: TlsSocket, mut remain: usize)
    {
        use std::io::{Read,ErrorKind};

        let mut buf = Vec::with_capacity(4096);
        buf.resize(4096, 0);
        match sock.read(&mut buf) {
            Ok(len) => {
                if len > remain {
                    panic!("{} > {}", len, remain);
                }
                self.body.as_mut().unwrap().extend_from_slice(&buf[0..len]);
                remain -= len;
                if remain > 0 {
                    self.stage = Stage::S4(sock,remain);
                    self.status = Status::InProgress;
                }
                else {
                    self.stage = Stage::S5;
                    self.status = Status::Done;
                }
            }
            Err(e) => {
                match e.kind() {
                    ErrorKind::WouldBlock => {
                        self.stage = Stage::S4(sock,remain);
                        self.status = Status::InProgress;
                    }
                    _ => {
                        self.stage = Stage::SE;
                        self.status = Status::Bad(BadStatus::Connection);
                    }
                }
            }
        }
    }
}

